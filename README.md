# PERN Stack Course FreeCodeCamp

![Project-Course](https://img.shields.io/badge/Project-Course-yellow.svg)
![Status-Finished](https://img.shields.io/badge/Status-Finished-blue.svg)
![Maintained-No](https://img.shields.io/badge/Maintained-No-red.svg)
<a href="https://gitlab.com/bg90dev-courses/pern-stack-course-freecodecamp" alt="GitLab Repository Link">
  <img alt="gitlab repo" src="https://img.shields.io/badge/gitlab-repo-purple?logo=gitlab"/>
</a>
<a href="https://es.reactjs.org/" alt="Documentation Link">
  <img alt="Documentation Link" src="https://img.shields.io/badge/Made_with-ReactJS-cyan"/>
</a>

Curso de Stack PERN (Postgres, Express, ReactJS, NodeJS) de FreeCodeCamp 
https://www.youtube.com/watch?v=ldYcgPKEZC8

## Built With
### Backend
<a href="https://www.javascript.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/javascript.jpeg" width=50 alt="JavaScript"></a>
<a href="https://nodejs.org/es/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/nodejs.png" width=50 alt="NodeJS"></a>
<a href="https://expressjs.com/es/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/expressJS.png" width=50 alt="ExpressJS"></a>
### Frontend
<a href="https://getbootstrap.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/bootstrap.png" width=50 alt="BootStrap"></a>
<a href="https://es.reactjs.org/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/reactJs.png" width=50 alt="React"></a>
### Platforms
<a href="https://code.visualstudio.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/vscode.png" width=50 alt="VSCode"></a>

## Author
* **Borja Gete**

<a href="mailto:borjag90dev@gmail.com" alt="Borja Gete mail"><img src="https://img.shields.io/badge/borjag90dev@gmail.com-DDDDDD?style=for-the-badge&logo=gmail" title="Go To mail" alt="Borja Gete mail"/></a> <a href="https://gitlab.com/BorjaG90" alt="Borja Gete Gitlab"><img src="https://img.shields.io/badge/BorjaG90-purple?style=for-the-badge&logo=gitlab" title="Go To Github Profile" alt="Borja Gete Github"/></a> <a href="https://github.com/BorjaG90" alt="Borja Gete Github"><img src="https://img.shields.io/badge/BorjaG90-black?style=for-the-badge&logo=github" title="Go To Github Profile" alt="Borja Gete Github"/></a> <a href="https://linkedin.com/in/borjag90" alt="Borja Gete LinkedIn"><img src="https://img.shields.io/badge/BorjaG90-blue?style=for-the-badge&logo=linkedin" title="Go To LinkedIn Profile" alt="Borja Gete LinkedIn"/></a>
